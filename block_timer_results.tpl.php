<?php
/**
 * Template to display results for each block
 */
?>
<div class="block-timer-result">
  <div class="summary <?php print $results->highlight ?>">
    <?php print $results->fields['slow_rank']['value'] ?>. Rendered in <?php print $results->fields['rendering_time']['value'] ?> 
  </div>
  <div class="detail <?php print $results->highlight ?>">
    <table>
      <?php foreach ($results->fields as $field): ?>
        <tr><th><?php print $field['label'] ?></th><td><?php print $field['value'] ?></td></tr>  
      <?php endforeach; ?>
    </table>  
  </div>
</div>