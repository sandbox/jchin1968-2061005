/**
 * @file
 * Show/hide Block Timer detail when mouse hover/unhover over
 * a Block Timer summary box
 */

(function ($) {
  Drupal.behaviors.block_timer =  {
    attach: function(context, settings) {
      // When mouse hover/unhover over the block_timer summary, show/hide the details block	
      $(".block-timer-result .summary").hover(
        function () {        	
          $(this).siblings('.detail').show();
        },
        function () {
          $(this).siblings('.detail').hide();
        }
      );
    }
  };	
})(jQuery);