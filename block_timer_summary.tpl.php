<?php
/**
 * Template to display summary
 */
?>
<div id="block-timer-summary">
  <h2><?php print $title ?></h2>
  <ul>
    <li><?php print $summary ?></li>
    <?php if ($not_displayed) : ?>
    <li><?php print $not_displayed_heading ?></li>
      <ul>
        <?php foreach ($not_displayed as $block): ?>
        <li><?php print $block ?></li>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  </ul>  
</div>
